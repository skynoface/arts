package sout.top.design;

/**
 * @author jiaheming
 * @date 2020/4/16 17:25
 * contact jiaheming2006@126.com
 */
public abstract class AbstractOrderToPayOrderDailyFlow {

    abstract OrderDailyFlowProcessDTO validCheckAndWrapProcessDTO(Long orderId);

    abstract void preprocess(OrderDailyFlowProcessDTO dto);

    abstract void calculate(OrderDailyFlowProcessDTO dto);

    abstract void process(OrderDailyFlowProcessDTO dto);
}

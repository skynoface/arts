package sout.top.weekSix;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.PriorityQueue;

/**
 * @author jiaheming
 * @date 2019-06-27 17:50
 * contact jiaheming2006@126.com
 */
public class SlideWindow {

    public static void main(String[] args) {
        int[] ints = maxSlidingWindow(new int[]{9, 10, 9, -7, -4, -8, 2, -6}, 5);
        for (int anInt : ints) {
            System.out.println(anInt);
        }
//        [9,10,9,-7,-4,-8,2,-6]
//        5

    }


    public static int[] maxSlidingWindow(int[] nums, int k) {
        if (nums == null) {
            return new int[0];
        }
        if (nums.length == 0 || k == 0) {
            return new int[0];
        }
        int[] result = new int[nums.length - k + 1];
        int tempi = 0;
        PriorityQueue<Integer> maxHeap = new PriorityQueue<>(11, (i1, i2) -> i2 - i1);

        for (int i = 0; i < nums.length; i++) {
            int num = nums[i];
            if (maxHeap.size() < k - 1) {
                maxHeap.offer(num);
                continue;
            }
            if (maxHeap.size() == k) {
                maxHeap.remove(nums[i - k]);
            }
            maxHeap.offer(num);
            result[tempi++] = maxHeap.peek();
        }
        return result;

//        ArrayDeque<Integer> deque = new ArrayDeque<>();
//        for (int num : nums) {
//            deque.pop();
//        }

//        return null;
    }

    /**
     * TODO dequeue 实现
     * @author jiaheming
     * @date 2019-07-01 10:14
     * @return {@link int[]}
     **/
    public static int[] dequeue(int[] nums, int k) {
        Deque<Integer> deque = new ArrayDeque<>();
//        for (int num : nums) {
//            deque.pop();
//        }

        return null;
    }
}

package sout.top.weekthree;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 * @author jiaheming
 * @date 2019-04-21 09:27
 * contact jiaheming2006@126.com
 */
public class ValidParentheses {
    public static void main(String[] args) {
        System.out.println(mySolution("({[})"));
    }

    /**
     * 首先想到了栈先进后出
     * 先peek，如果不存在 直接 push
     * 如果存在，判断是否和之前的构成闭合，如果闭合 pop。
     * 最后 stack 空则说明是合法的括号
     *
     * @param s
     * @return
     */
    public static boolean mySolution(String s) {
        char[] chars = s.toCharArray();
        Stack<String> tempStack = new Stack<>();
        for (int i = 0; i < chars.length; i++) {
            if (!tempStack.empty()) {
                String peek = tempStack.peek();
                if (peek != null) {
                    if (filter(peek, chars[i] + "")) {
                        tempStack.pop();
                        continue;
                    }
                }
            }
            tempStack.push(chars[i] + "");
        }
        return tempStack.empty();
    }

    private static boolean filter(String start, String end) {
        if (start.equals("(") && end.equals(")")) {
            return true;
        } else if (start.equals("{") && end.equals("}")) {
            return true;
        } else if (start.equals("[") && end.equals("]")) {
            return true;
        }
        return false;
    }

    public static boolean officialSolution(String s) {
        Map<Character, Character> rules = new HashMap<>();
        rules.put(')', '(');
        rules.put('}', '{');
        rules.put(']', '[');

        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (!rules.containsKey(c)) {
                stack.push(c);
            } else {
                char pop = stack.empty() ? '#' : stack.pop();
                if (pop != rules.get(c)) {
                    return false;
                }
            }
        }
        return stack.isEmpty();
    }
}

package sout.top.java.error;

/**
 * @author jiaheming
 * @date 2020/5/13 12:28
 * contact jiaheming2006@126.com
 */
public class ErrorTest {
    public static void main(String[] args) {
        try {
            Integer num = null;
            num++;
        } finally {
            System.out.println("finally");
        }

    }
}

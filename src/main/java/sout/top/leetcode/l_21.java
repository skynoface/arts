package sout.top.leetcode;

import javafx.beans.binding.When;
import sout.top.common.help.ListNode;

/**
 * 将两个升序链表合并为一个新的升序链表并返回。新链表是通过拼接给定的两个链表的所有节点组成的。 
 * <p>
 * 示例：
 * <p>
 * 输入：1->2->4, 1->3->4
 * 输出：1->1->2->3->4->4
 * <p>
 *
 * @author jiaheming
 * @date 2020/5/12 17:12
 * contact jiaheming2006@126.com
 */
public class l_21 {

    public static void main(String[] args) {
        ListNode listNode = new ListNode(1);
        listNode.append(2);
        listNode.next.append(4);

        ListNode listNode2 = new ListNode(5);
//        listNode2.append(3);
//        listNode2.next.append(4);

        System.out.println(listNode);
        System.out.println(listNode2);

//        System.out.println(mergeTwoLists(listNode, listNode2));
        System.out.println(暴力(listNode, listNode2));
    }

    public static ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        if(l1 == null && l2 != null) return l2;
        if(l2 == null && l1 != null) return l1;
        if(l1 == null) return null;

        Integer val = l1.val;
        Integer val1 = l2.val;

        ListNode next1 = l1.next;
        ListNode next2 = l2.next;

        ListNode dummyHead = new ListNode(0);
        ListNode cur = dummyHead;

        while (true) {
            cur.next =  new ListNode(Math.min(initIntegerDefaultZero(val), initIntegerDefaultZero(val1)));
            cur.next.next =  new ListNode(Math.max(initIntegerDefaultZero(val), initIntegerDefaultZero(val1)));
            cur = cur.next.next;
            System.out.println("cur:" + cur);
            System.out.println("dummyHead:" + dummyHead);
            if (next1 == null && next2 != null) {
                cur.next = next2;
                break;
            }
            if (next1 != null && next2 == null) {
                cur.next = next1;
                break;
            }
            if (next1 == null) {
                break;
            }
            val = next1.val;
            next1 = next1.next;
            val1 = next2.val;
            next2 = next2.next;
        }
        return dummyHead.next;
    }

    private static int initIntegerDefaultZero(Integer val) {
        if (val == null) {
            return 0;
        }
        return val;
    }



    private static ListNode 暴力(ListNode l1, ListNode l2){
        ListNode prePrev = new ListNode(0);
        ListNode pre = prePrev;

        while (l1 != null && l2 != null){
            if (l1.val <= l2.val){
                pre.next = l1;
                l1 = l1.next;
            }else{
                pre.next = l2;
                l2 = l2.next;
            }
            pre = pre.next;
        }
        // 合并后 l1 和 l2 最多只有一个还未被合并完，我们直接将链表末尾指向未合并完的链表即可
        pre.next = l1 == null ? l2 : l1;

        return prePrev.next;
    }




}

package sout.top.weekOne;

import java.util.HashMap;
import java.util.Map;

/**
 * 要善于使用map来解决计算过程中的问题
 *
 * @author jiaheming
 * @date 2019-03-13 15:44
 * contact jiaheming2006@126.com
 */
public class TwoSum {
    public static void main(String[] args) {
        int[] ints = twoSum(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15}, 29);
        for (int anInt : ints) {
            System.out.print(anInt + " ");
        }
        int[] ints1 = twoSum2(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15}, 29);
        System.out.println();
        for (int anInt : ints1) {
            System.out.print(anInt + " ");
        }
        System.out.println();
    }


    public static int[] empty(int[] nums, int target) {
        // key value value index
        Map<Integer, Integer> result = new HashMap<>(16);
        for (int i = 0; i < nums.length; i++) {
            int num = nums[i];
            int rest = target - num;
            if (result.containsKey(rest) && result.get(rest) != i) {
                return new int[]{result.get(rest), i};
            }
            result.put(num, i);
        }
        throw new IllegalArgumentException("No two sum solution");
    }


    /**
     * @param target every target can use nums[x] + nums[y] to represent
     */
    public static int[] twoSum(int[] nums, int target) {
        long start = System.currentTimeMillis();
        for (int i = 0; i < nums.length; i++) {
            for (int j = i + 1; j < nums.length; j++) {
                if (nums[i] + nums[j] == target) {
                    long end = System.currentTimeMillis();
                    System.out.println(end - start + " ms");
                    return new int[]{i, j};
                }
            }
        }
        return null;
    }


    public static int[] twoSum2(int[] nums, int target) {
        Map<Integer, Integer> map = new HashMap<>(16);
        for (int i = 0; i < nums.length; i++) {
            map.put(nums[i], i);
        }
        for (int i = 0; i < nums.length; i++) {
            int complement = target - nums[i];
            if (map.containsKey(complement) && map.get(complement) != i) {
                return new int[]{i, map.get(complement)};
            }
        }
        throw new IllegalArgumentException("No two sum solution");
    }

    public int[] twoSum3(int[] nums, int target) {
        Map<Integer, Integer> map = new HashMap<>(16);
        for (int i = 0; i < nums.length; i++) {
            int complement = target - nums[i];
            if (map.containsKey(complement)) {
                return new int[]{map.get(complement), i};
            }
            map.put(nums[i], i);
        }
        throw new IllegalArgumentException("No two sum solution");
    }
}

package sout.top.weekfour;

import java.util.Stack;

/**
 * @author jiaheming
 * @date 2019-05-02 16:06
 * contact jiaheming2006@126.com
 */
public class ImplementQueueUsingStack {

    public static void main(String[] args) {
        MyQueue obj = new MyQueue();
        obj.push(2);
        obj.push(3);
        int param_2 = obj.pop();
        int param_5 = obj.pop();
        int param_3 = obj.peek();
        boolean param_4 = obj.empty();
    }


    static class MyQueue {

        private Stack<Integer> temp;
        private Stack<Integer> result;

        /**
         * Initialize your data structure here.
         */
        public MyQueue() {
            temp = new Stack();
            result = new Stack();
        }

        /**
         * Push element x to the back of queue.
         */
        public void push(int x) {
            if (result.isEmpty()) {
                result.push(x);
            } else {
                while (!result.isEmpty()) {
                    temp.push(result.pop());
                }
                temp.push(x);
                while (!temp.isEmpty()) {
                    result.push(temp.pop());
                }
            }
        }

        /**
         * Removes the element from in front of queue and returns that element.
         */
        public int pop() {
            return result.pop();
        }

        /**
         * Get the front element.
         */
        public int peek() {
            if (result.isEmpty()) return 0;
            return result.peek();
        }

        /**
         * Returns whether the queue is empty.
         */
        public boolean empty() {
            return result.empty();
        }
    }
}

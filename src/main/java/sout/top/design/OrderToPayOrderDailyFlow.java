package sout.top.design;

/**
 * @author jiaheming
 * @date 2020/4/16 17:26
 * contact jiaheming2006@126.com
 */
public class OrderToPayOrderDailyFlow extends AbstractOrderToPayOrderDailyFlow {

    public void invoke(Long orderId){

    }

    @Override
    OrderDailyFlowProcessDTO validCheckAndWrapProcessDTO(Long orderId) {
        return null;
    }

    @Override
    void preprocess(OrderDailyFlowProcessDTO dto) {

    }

    @Override
    void calculate(OrderDailyFlowProcessDTO dto) {

    }

    @Override
    void process(OrderDailyFlowProcessDTO dto) {

    }
}

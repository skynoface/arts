package sout.top.java.map;


/**
 * @param <K> the type of keys maintained by this map
 * @param <V> the type of mapped values
 * @author jiaheming
 * @since 2019/12/26 2:38 下午
 */
public interface TopMap<K, V> {
    
    int size();
    
    void put(K k, V v);
    
    void remove(K k);
    
    V get(K k);
    
    void clear();
    
    boolean isEmpty();
}

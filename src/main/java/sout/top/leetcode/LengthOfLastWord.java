package sout.top.leetcode;

/**
 * @author jiaheming
 * @since 2019/10/9 9:42 上午
 */
public class LengthOfLastWord {
    
    public static void main(String[] args) {
        System.out.println(lengthOfLastWord2("hello world"));
    }
    
    public static int lengthOfLastWord(String s) {
        char[] chars = s.toCharArray();
        int size = s.length();
        int rest = 0;
        for (int i = size - 1; i >= 0; i--) {
            if (rest == 0 && chars[i] == ' ') {
                continue;
            } else if (chars[i] == ' ') {
                break;
            }
            
            rest++;
        }
        return rest;
    }
    
    public static int lengthOfLastWord2(String s) {
        int end = s.length() - 1;
        while (end >= 0 && s.charAt(end) == ' ') {
            end--;
        }
        if (end < 0) {
            return 0;
        }
        int start = end;
        while (start >= 0 && s.charAt(start) != ' ') {
            start--;
        }
        return end - start;
    }
}

package sout.top.leetcode;

/**
 * 122. Best Time to Buy and Sell Stock II
 *
 * @author jiaheming
 * @since 2019/9/19 8:55 上午
 */
public class BestTimeToBuyAndSellStock3 {
    public static void main(String[] args) {
    }
    
    public static int maxProfit2(int[] prices) {
        int minPrice = Integer.MAX_VALUE;
        int maxProfit = 0;
        for (int i = 0; i < prices.length ; i++) {
            if (prices[i] < minPrice){
                minPrice = prices[i];
            }else if(prices[i] - minPrice > maxProfit){
                maxProfit = prices[i] - minPrice;
            }
        }
        return maxProfit;
    }
    
}

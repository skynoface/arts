package sout.top.common.help;

import java.util.List;

/**
 * @author jiaheming
 * @date 2019-03-13 16:05
 * contact jiaheming2006@126.com
 */
public class ListNode {

    public int val;

    public ListNode next;

    public ListNode(int x) {
        val = x;
    }

    public static ListNode of(int x) {
        return new ListNode(x);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(val);
        if (next != null) {
            recursion(next, sb);
        }
        return sb.toString();
    }

    private void recursion(ListNode node, StringBuilder sb) {
        sb.append("->").append(node.val);
        if (node.next != null) {
            recursion(node.next, sb);
        }
    }

    public static void buildNode(ListNode listNode, int val, int max) {
        listNode.next = new ListNode(val);
        if (val != max) {
            buildNode(listNode.next, val + 1, max);
        }
    }

    public void append(int val) {
        this.next = new ListNode(val);
    }
}

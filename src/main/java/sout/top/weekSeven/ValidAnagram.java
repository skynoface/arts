package sout.top.weekSeven;

import java.util.HashMap;
import java.util.Map;

/**
 * @author jiaheming
 * @date 2019-07-01 10:31
 * contact jiaheming2006@126.com
 */
public class ValidAnagram {
    public static void main(String[] args) {
        char test = 'A';
        char test2 = 'b';
        System.out.println('A');
        char[] chars = {'t', 'a', 'r'};
        quickSort(chars, 0, chars.length - 1);

        System.out.println(chars);
    }

    public boolean isAnagram(String s, String t) {


        if (s.length() != t.length()) {
            return false;
        }
        char[] sourceChars = s.toCharArray();
        char[] targetChars = t.toCharArray();


        Map<Character, Integer> source = new HashMap<>(16);
        Map<Character, Integer> target = new HashMap<>(16);

        for (char sourceChar : sourceChars) {
            Integer count = source.get(sourceChar);
            if (count != null) {
                source.put(sourceChar, ++count);
                continue;
            }
            source.put(sourceChar, 0);
        }
        for (char targetChar : targetChars) {
            Integer count = target.get(targetChar);
            if (count != null) {
                target.put(targetChar, ++count);
                continue;
            }
            target.put(targetChar, 0);
        }

        for (Map.Entry<Character, Integer> sourceEntry : source.entrySet()) {
            Character key = sourceEntry.getKey();
            Integer value = sourceEntry.getValue();

            if (target.containsKey(key) && !target.get(key).equals(value)) {
                return false;
            }
        }

        return true;
    }

    private static int count;

    private static void quickSort(char[] num, int left, int right) {
        //如果left等于right，即数组只有一个元素，直接返回
        if (left >= right) {
            return;
        }
        //设置最左边的元素为基准值
        char key = num[left];
        //数组中比key小的放在左边，比key大的放在右边，key值下标为i
        int i = left;
        int j = right;
        while (i < j) {
            //j向左移，直到遇到比key小的值
            while (num[j] >= key && i < j) {
                j--;
            }
            //i向右移，直到遇到比key大的值
            while (num[i] <= key && i < j) {
                i++;
            }
            //i和j指向的元素交换
            if (i < j) {
                char temp = num[i];
                num[i] = num[j];
                num[j] = temp;
            }
        }
        num[left] = num[i];
        num[i] = key;
        count++;
        quickSort(num, left, i - 1);
        quickSort(num, i + 1, right);
    }

}

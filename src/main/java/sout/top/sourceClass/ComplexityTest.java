package sout.top.sourceClass;

/**
 * @author jiaheming
 * @date 2019-06-28 15:17
 * contact jiaheming2006@126.com
 */
public class ComplexityTest {

    public static void main(String[] args) {
        int value = 0;
        for (int i = 0; i < 5; i++) {
//            System.out.println(i);
            for (int j = 0; j < 5; j++) {
                System.out.println(++value);
            }
        }
    }
}

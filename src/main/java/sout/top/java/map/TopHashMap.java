package sout.top.java.map;

import java.util.Map;

/**
 * @param <K> the type of keys maintained by this map
 * @param <V> the type of mapped values
 * @author jiaheming
 * @since 2019/12/26 1:28 下午
 */
public class TopHashMap<K, V> implements TopMap<K, V> {
    
    private Node<K, V>[] entries;
    
    class Node<K, V> implements Map.Entry<K, V> {
        
        private K key;
        
        private V value;
        
        //        private Node<K, V> next;
        
        @Override
        public K getKey() {
            return key;
        }
        
        @Override
        public V getValue() {
            return value;
        }
        
        @Override
        public V setValue(V value) {
            return value;
        }
    }
    
    @Override
    public int size() {
        return 0;
    }
    
    @Override
    public void put(K k, V v) {
    
    }
    
    @Override
    public void remove(K k) {
    
    }
    
    @Override
    public V get(K k) {
        return null;
    }
    
    @Override
    public void clear() {
    
    }
    
    @Override
    public boolean isEmpty() {
        return false;
    }
}

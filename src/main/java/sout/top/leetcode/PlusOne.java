package sout.top.leetcode;

import java.lang.reflect.Array;
import java.util.Arrays;

/**
 * @author jiaheming
 * @since 2019/10/11 2:55 下午
 */
public class PlusOne {
    
    public static void main(String[] args) {
        int[] ints = { 1, 9, 9 };
        int[] ints1 = plusOneV2(ints);
        for (int anInt : ints1) {
            System.out.println(anInt);
        }
    }
    
    public static int[] plusOneV2(int[] digits) {
        for (int i = digits.length - 1; i >= 0; i--) {
            digits[i]++;
            digits[i] = digits[i] % 10;
            if (digits[i] != 0) {
                return digits;
            }
        }
        digits = new int[digits.length + 1];
        digits[0] = 1;
        return digits;
    }
    
    public static int[] plusOne(int[] digits) {
        int carry = 0;
        int cur;
        int[] result = new int[digits.length];
        for (int i = digits.length - 1; i >= 0; i--) {
            int digit = digits[i];
            int add = digit;
            if (i == digits.length - 1) {
                add = digit + 1;
            }
            if (carry != 0) {
                add += carry;
            }
            if (add >= 10) {
                cur = add - 10;
                carry = 1;
            } else {
                carry = 0;
                cur = add;
            }
            result[i] = cur;
        }
        if (carry != 0) {
            result = Arrays.copyOf(result, result.length + 1);
            result[0] = carry;
        }
        return result;
    }
}

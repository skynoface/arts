package sout.top.leetcode;

/**
 * 122. Best Time to Buy and Sell Stock II
 *
 * @author jiaheming
 * @since 2019/9/19 8:55 上午
 */
public class BestTimeToBuyAndSellStock2 {
    public static void main(String[] args) {
        System.out.println(maxProfit(new int[] {1, 7, 2, 3, 6, 7, 6, 7}));
        System.out.println(maxProfit2(new int[] { 1, 7, 2, 3, 6, 7, 6, 7}));
    }
    
    public static int maxProfit(int[] prices) {
        return calculate(prices, 0);
    }
    
    /**
     * [7, 1, 5, 3, 6, 4]
     *
     * @param prices
     * @param s
     * @return
     */
    public static int calculate(int[] prices, int s) {
        if (s >= prices.length)
            return 0;
        int max = 0;
        for (int start = s; start < prices.length; start++) {
            int maxprofit = 0;
            for (int i = start + 1; i < prices.length; i++) {
                if (prices[start] < prices[i]) {
                    int profit = calculate(prices, i + 1) + prices[i] - prices[start];
                    if (profit > maxprofit)
                        maxprofit = profit;
                }
            }
            if (maxprofit > max)
                max = maxprofit;
        }
        return max;
    }
    
    public static int maxProfit2(int[] prices) {
        int maxprofit = 0;
        for (int i = 1; i < prices.length; i++) {
            if (prices[i] > prices[i - 1])
                maxprofit += prices[i] - prices[i - 1];
        }
        return maxprofit;
    }
    
}

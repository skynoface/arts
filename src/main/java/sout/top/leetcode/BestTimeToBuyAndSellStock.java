package sout.top.leetcode;

/**
 * 121 . Best Time to Buy and Sell Stock
 *
 * @author jiaheming
 * @since 2019/9/19 8:55 上午
 */
public class BestTimeToBuyAndSellStock {
    public static void main(String[] args) {
        System.out.println(maxProfit2(new int[] { 4, 3, 2, 5, 1 }));
    }
    
    /**
     * [7,1,5,3,6,4]
     * 5
     * [7,6,4,3,1]
     * 0
     *
     * @param prices
     * @return
     */
    public int maxProfit(int[] prices) {
        int maxProfit = 0;
        for (int i = 0; i < prices.length - 1; i++) {
            for (int j = i + 1; j < prices.length; j++) {
                int profit = prices[j] - prices[i];
                if (maxProfit < profit) {
                    maxProfit = profit;
                }
            }
        }
        return maxProfit;
    }
    
    /**
     * dp[i][k][j]
     * i 天数、k 购买的次数、j 当前的持股状态
     * i 0 -> n-1
     * k = 1
     * j in 0,1
     *
     * @param prices
     * @return
     */
    public static int maxProfit2(int[] prices) {
        int dpI0 =0;
        int dpI1 = Integer.MIN_VALUE;
        for (int i = 0; i < prices.length; i++) {
            dpI0 = Math.max(dpI0,dpI1+prices[i]);
            dpI1 = Math.max(dpI1,dpI0-prices[i]);
        }
        return dpI0;
//        int minPrice = Integer.MAX_VALUE;
//        int maxProfit = 0;
//        for (int i = 0; i < prices.length; i++) {
//            if (prices[i] < minPrice) {
//                minPrice = prices[i];
//            } else if (prices[i] - minPrice > maxProfit) {
//                maxProfit = prices[i] - minPrice;
//            }
//        }
//        return maxProfit;
    }
}

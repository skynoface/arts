package sout.top.weekTwo;

import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.concurrent.TimeUnit;

/**
 * @author jiaheming
 * @date 2019-04-14 11:20
 * contact jiaheming2006@126.com
 */
@Warmup(iterations = 10, time = 1)
@Measurement(iterations = 5, time = 1)
//-XX:LoopUnrollLimit=1
@Fork(value = 1, jvmArgsPrepend = {"-XX:LoopUnrollLimit=1"})
@State(Scope.Benchmark)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
public class LockRoach {


    public static void main(String[] args) throws RunnerException {
        Options build = new OptionsBuilder().include(LockRoach.class.getName()).forks(1).build();
        new Runner(build).run();
    }

    int x;


    @Benchmark
    @CompilerControl(CompilerControl.Mode.DONT_INLINE)
    public void test() {
        for (int c = 0; c < 1000; c++) {
            synchronized (this) {
                x += 0x42;
            }
        }
    }

}



package sout.top.weekTwo;

import sout.top.common.help.ListNode;

/**
 * @author jiaheming
 * @date 2019-03-13 16:03
 * contact jiaheming2006@126.com
 */
public class AddTwoNumbers {
    public static void main(String[] args) {
        ListNode listNode = new ListNode(1);
        ListNode.buildNode(listNode, 2, 3);

        ListNode listNode2 = new ListNode(2);
        ListNode.buildNode(listNode2, 3, 3);
        System.out.println(listNode.toString());
        System.out.println(listNode2.toString());
        System.out.println(addTwoNumbers(listNode, listNode2).toString());
        System.out.println(ofSolution(listNode, listNode2).toString());
    }

    // =====official solution =====

    /**
     * 逐位相加，如果相加结果大于10，产生下一个节点。下一位的相加要加上这个进位值
     * <p>
     * curr 结果链表
     * 提炼出来的条件 l1.val + l2.val = result
     * result / 10 = carry
     * result % 10 = curr.next
     *
     * @param l1
     * @param l2
     * @return
     */
    public static ListNode ofSolution(ListNode l1, ListNode l2) {
        // 返回结果
        ListNode res = new ListNode(0);
        ListNode p = l1, q = l2, curr = res;
        // 进位
        int carry = 0;
        while (p != null || q != null) {
            int x = p != null ? p.val : 0;
            int y = q != null ? q.val : 0;
            int result = carry + x + y;
            carry = result / 10;
            curr.next = new ListNode(result % 10);
            // 这里已经移除最开始的指引，指向了下一个节点
            // 最开始这里迷糊了一阵，一直认为curr一直和res指向同一个对象呢。
            curr = curr.next;
            if (p != null) p = p.next;
            if (q != null) q = q.next;
        }

        if (carry > 0) {
            curr.next = new ListNode(carry);
        }
        return res.next;
    }

    // =====official solution ====

    //region my solution
    private static ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        //创建新的ListNode
        ListNode listNode = new ListNode(0);
        //参与运算的两个数均不为空
        if (null != l1 && null != l2) {
            //进位标识
            boolean needUp = false;
            //第一位计算
            int i1 = l1.val + l2.val;
            if (i1 >= 10) {
                i1 = i1 - 10;
                needUp = true;
            }
            listNode.val = i1;
            //进位
            if (needUp) {
                listNode.next = new ListNode(1);
            }
            //第二位
            recursive(l1, l2, listNode);
            return listNode;
        }
        //l1 is not null,l2 is null.
        else if (null != l1) {
            return l1;
        }
        //l2 is null ,l1 is not null
        else if (null != l2) {
            return l2;
        } else {
            //wtf
            return listNode;
        }
    }

    private static void recursive(ListNode l1, ListNode l2, ListNode listNode) {
        boolean needUp;
        if (null != l1 && null != l2 && null != l1.next && null != l2.next) {
            needUp = false;
            int i2 = l1.next.val + l2.next.val;
            if (i2 >= 10) {
                i2 = i2 - 10;
                needUp = true;
            }
            if (null != listNode.next) {
                if (needUp) {
                    listNode.next.next = new ListNode(1);
                }
                needUp = false;
                int i2e = listNode.next.val + i2;
                if (i2e >= 10) {
                    i2e = i2e - 10;
                    needUp = true;
                }
                listNode.next.val = i2e;
                if (needUp) {
                    listNode.next.next = new ListNode(null != listNode.next.next ? 2 : 1);
                }
            } else {
                listNode.next = new ListNode(i2);
                if (needUp) {
                    listNode.next.next = new ListNode(1);
                }
            }
            recursive(l1.next, l2.next, listNode.next);
        } else if (null != l1 && null != l1.next) {
            calSinglePlace(l1, listNode);
            recursive(l1.next, null, listNode.next);
        } else if (null != l2 && null != l2.next) {
            calSinglePlace(l2, listNode);
            recursive(null, l2.next, listNode.next);
        }
    }

    private static void calSinglePlace(ListNode l1, ListNode listNode) {
        boolean needUp;
        needUp = false;
        if (null != listNode.next) {
            int in1 = listNode.next.val + l1.next.val;
            if (in1 >= 10) {
                in1 = in1 - 10;
                needUp = true;
            }
            listNode.next.val = in1;
            if (needUp) {
                listNode.next.next = new ListNode(1);
            }
        } else {
            listNode.next = new ListNode(l1.next.val);
        }
    }
//endregion

}